# Ensure Time Zone and Region.
class { 'timezone':
timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
}

class { '::mysql::server':
  root_password           => 'vagrantpass',
  remove_default_accounts => true,
  override_options        => $override_options
}

mysql::db { 'mpwar_test':
  user     => 'mpwardb',
  password => 'mpwardb',
  host     => 'localhost',
}
mysql::db { 'mympwar':
  user     => 'mpwardb',
  password => 'mpwardb',
  host     => 'localhost',
}


