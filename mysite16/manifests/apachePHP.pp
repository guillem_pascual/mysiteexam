class { 'apache': }
class { 'yum':
    extrarepo => [ 'epel' ],
}

apache::vhost {'myMpwar.prod':
				port            =>'80',
				docroot         => '/var/www/myproject',
				docroot_owner   => 'vagrant',
				docroot_group   => 'vagrant',
}

apache::vhost {'myMpwar.dev':
				port            =>'80',
				docroot         => '/var/www/myproject',
				docroot_owner   => 'vagrant',
				docroot_group   => 'vagrant',
}

# Ensure Time Zone and Region.
class { 'timezone':
timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
}

# Create files
include mysite16::createfiles

# PHP
include ::yum::repo::remi
package { 'libzip-last':
  require => Yumrepo['remi']
}

$php_version = '56'

# remi_php55 requires the remi repo as well
if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}
  # remi_php56 requires the remi repo as well
elsif $php_version == '56' {
  $yum_repo = 'remi-php56'
  class{'::yum::repo::remi_php56':
    require => Package['libzip-last']
  }
}
# version 5.4
elsif $php_version == '54' {
  $yum_repo = 'remi'
  include ::yum::repo::remi
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo],
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'pecl-memcache', 'soap' ]: }

include memcached
