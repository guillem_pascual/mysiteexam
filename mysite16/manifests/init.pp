# Ensure Time Zone and Region.
class { 'timezone':
timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
server => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
}

# Apache, PHP y creación de ficheros
include mysite16::apachePHP

# MySQL
include mysite16::mysql

# MongoDB
include mysite16::mongodb
