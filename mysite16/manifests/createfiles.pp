class mysite16::createfiles
{
    file { '/var/www/myproject/index.php':
        ensure  => 'present',
        owner => "apache",
        group => "apache",
        mode    => '0644',
        replace => true,
        content => " Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease} ",
    }
    file { '/var/www/project1/info.php':
        ensure  => 'present',
        owner => "apache",
        group => "apache",
        mode    => '0644',
        replace => true,
        source => "puppet:///modules/mysite16/info.php",
    }
}
